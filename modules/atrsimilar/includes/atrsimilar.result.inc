<?php

/**
 * @file
 *   Similarity review results.
 */

/**
 * Cound how many results there are for a review.
 *
 * @param $rid
 *   The RID of the review for which to count the results.
 *
 * @return
 *   An integer with the amount of results.
 */
function atrsimilar_result_count($rid) {
  $count = db_result(db_query("SELECT COUNT(*)
  FROM {atrsimilar_string} ss
  JOIN {atr_string} s_a ON ss.sid_a = s_a.sid
  JOIN {atr_string} s_b ON ss.sid_b = s_b.sid
  WHERE s_a.rid = %d", $rid));

  return array(
    'number' => $count,
    'text' => t('!count similar texts', array('!count' => $count)),
  );
}

/**
 * Display an overview of similar strings.
 */
function atrsimilar_result($review) {
  drupal_add_js(drupal_get_path('module', 'atr') . '/js/atr.js');
  drupal_add_css(drupal_get_path('module', 'atr') . '/css/atr.css');
  drupal_add_css(drupal_get_path('module', 'atrsimilar') . '/css/atrsimilar.css');
  module_load_include('inc', 'atrsimilar', 'includes/atrsimilar.review');

  $header = array(
    array(
      'data' => t('String'),
      'field' => 's_a.string',
    ),
    array(
      'data' => t('Similar string'),
      'field' => 's_b.string',
    ),
    array(
      'data' => t('Similarity'),
      'field' => 'similarity',
    ),
  );

  $rows = array();
  $result = pager_query("SELECT ss.*, s_a.string AS string_a, s_b.string AS string_b
  FROM {atrsimilar_string} ss
  JOIN {atr_string} s_a ON ss.sid_a = s_a.sid
  JOIN {atr_string} s_b ON ss.sid_b = s_b.sid
  WHERE s_a.rid = %d" . tablesort_sql($header), 25, 0, NULL, $review->rid);
  while ($string_data = db_fetch_array($result)) {
    list($sid_a, $sid_b, $similarity, $string_a, $string_b) = array_values($string_data);
    $diff = atrsimilar_diff($string_a, $string_b);
    $rows[] = array(
      theme('atr_string', atrsimilar_render_old($diff), atr_string_locations($sid_a)),
      theme('atr_string', atrsimilar_render_new($diff), atr_string_locations($sid_b)),
      $similarity . '%',
    );
  }

  $pager = theme('pager');

  return $pager . theme_table($header, $rows) . $pager;
}

/**
 * Render the old string with deletions.
 */
function atrsimilar_render_old($diff) {
  $rendered = '';

  foreach ($diff[0] as $i => $token) {
    $string = htmlspecialchars($token['string']);
    $rendered .= $token['common'] == TRUE ? $string : theme('atrsimilar_token_old', $string);
  }

  return $rendered;
}

/**
 * Render the new string with insertions.
 */
function atrsimilar_render_new($diff) {
  $rendered = '';

  foreach ($diff[1] as $i => $token) {
    $string = htmlspecialchars($token['string']);
    $rendered .= $token['common'] == TRUE ? $string : theme('atrsimilar_token_new', $string);
  }

  return $rendered;
}

/**
 * Theme a token from the old string.
 *
 * @param $token
 * The token to theme.
 */
function theme_atrsimilar_token_old($token) {
  return '<del class="atr-highlight atrsimilar error">' . $token . '</del>';
}

/**
 * Theme a token from the new string.
 *
 * @param $token
 * The token to theme.
 */
function theme_atrsimilar_token_new($token) {
  return '<ins class="atr-highlight atrsimilar ok">' . $token . '</ins>';
}