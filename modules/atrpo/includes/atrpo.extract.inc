<?php

/**
 * @file
 *   Extract texts from GNU gettext translations.
 */

/**
 * Extract strings from *.po files.
 *
 * @param $path
 *   The path to the *.po files.
 */
function atrpo_extract($path) {
  global $atr_strings;

  $po_files = atrpo_po_files($path);
  foreach ($po_files as $po_file) {
    atrpo_po_file_process($po_file);
  }

  // TODO: Generalise string location storage. Not all strings may be found in
  // files.
  if ($atr_strings) {
    module_load_include('inc', 'atr', 'includes/atr.extract');
    atr_filepath($atr_strings, $path);
  }
}

/**
 * Find all *.po files in a certain directory.
 *
 * @param $path
 *   The path from the Drupal root to the directory ATR should find strings in.
 *   Leading slashes are illegal. A trailing slash is required.
 *
 * @return
 *   An array containing all *.po files (path and file name).
 */
function atrpo_po_files($path) {
  $files = glob($path . '*.po');

  // Grab subdirectories.
  $dirs = glob($path .'*', GLOB_ONLYDIR);
  if (is_array($dirs)) {
    foreach ($dirs as $dir) {
      if (!preg_match('#(^|.+/)(CVS|.svn|.git)$#', $dir)) {
        $files = array_merge($files, atrpo_po_files($dir . '/'));
      }
    }
  }

  return $files;
}

/**
 * Extract and save strings from a *.po file.
 *
 * @param $file
 *   The *.po file to process.
 */
function atrpo_po_file_process($filepath) {
  global $atr_strings;

  require_once './includes/locale.inc';

  // Locale takes Drupal file objects. We haven't uploaded a file, so we create
  // a dummy object with only required properties.
  $file = (object) array(
    'filename' => basename($filepath),
    'filepath' => $filepath,
  );
  _locale_import_read_po('mem-store', $file);

  $strings = _locale_import_one_string('mem-report');
  foreach ($strings as $msgid => $string) {
    if($msgid) {
      // Locale converts singular and plural forms of the same string to arrays
      // with two items.
      foreach ((array) $string as $string) {
        $atr_strings[$string][] = array(
          'file' => $filepath,
          'line' => 0,
        );
      }
    }
  }
}