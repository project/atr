<?php

/**
 * @file
 *   Extract texts from Drupal code files.
 */

/**
 * Extract texts from code files.
 *
 * @param $path
 *   The path to the code files.
 * @param $api_version
 *   The verison of Drupal core the code is compatible with.
 */
function atrcode_extract($path, $api_version) {
  global $atr_strings;

  module_load_include('inc', 'potx', 'potx');

  potx_status('set', POTX_STATUS_SILENT);
  $code_files = _potx_explore_dir($path);
  foreach ($code_files as $code_file) {
    _potx_process_file($code_file, 0, '_atrcode_potx_save_string', '_atrcode_potx_save_version', $api_version);
  }

  // TODO: Generalise string location storage. Not all strings may be found in
  // files.
  if ($atr_strings) {
    module_load_include('inc', 'atr', 'includes/atr.extract');
    atr_filepath($atr_strings, $path);
  }
}

/**
 * Default $save_callback of POTX' _potx_process_file(). Saves values to global
 * arrays to reduce memory consumption problems when passing around big chunks
 * of values.
 *
 * @param $string
 *   The string that should be saved.
 * @param $file
 *   The name of the file where the string was found.
 * @param $line
 *   Line number where the string was found.
 * @param $string_mode
 *   String mode: POTX_STRING_INSTALLER, POTX_STRING_RUNTIME
 *   or POTX_STRING_BOTH.
 */
function _atrcode_potx_save_string($string, $file, $line = 0, $string_mode = POTX_STRING_RUNTIME) {
  global $atr_strings;

  $atr_strings[$string][] = array(
    'file' => $file,
    'line' => $line,
  );
}

/**
 * Default $version_callback of POTX' _potx_process_file().
 *
 * @param $version
 *   $file's CVS version number.
 * @param $file
 *   Name of file where the version information was found.
 */
function _atrcode_potx_save_version($version = NULL, $file = NULL) {
}