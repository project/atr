<?php

/**
 * @file
 *   Review strings.
 */

/**
 * Match strings against the blacklist.
 *
 * @param $profile
 *   The setings profile used for this review.
 * @param $context
 *   A batch operation context.
 */
function atrblacklist_review($review, $profile, &$context) {
  $context['message'] = t('Checking for blacklisted words or phrases.');

  $strings = array();
  $result = db_query("SELECT sid, string FROM {atr_string} WHERE rid = %d", $review->rid);
  while ($string = db_fetch_object($result)) {
    $strings[] = $string;
  }

  $values = array();
  $result = db_query("SELECT keyword FROM {atrblacklist_keyword} WHERE pid = %d", $profile->pid);
  while ($keyword = db_result($result)) {
    foreach ($strings as $string) {
      if (preg_match(atrblacklist_pattern($keyword), $string->string)) {
        $values[] = $string->sid;
        $values[] = $keyword;
      }
    }
  }

  if ($count = count($values)) {
    $placeholders = implode(',', array_fill(0, $count / 2, "(%d, '%s')"));
    db_query("INSERT INTO {atrblacklist_string} VALUES " . $placeholders, $values);
  }
}