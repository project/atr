<?php

/**
 * @file
 *   Blacklist review results.
 */

/**
 * Cound how many results there are for a review.
 *
 * @param $rid
 *   The RID of the review for which to count the results.
 *
 * @return
 *   An integer with the amount of results.
 */
function atrblacklist_result_count($rid) {
  $count = db_result(db_query("SELECT COUNT(*) FROM {atrblacklist_string} abs LEFT JOIN {atr_string} s ON abs.sid = s.sid WHERE s.rid = %d", $rid));

  return array(
    'number' => $count,
    'text' => t('!count matches', array('!count' => $count)),
  );
}

/**
 * Display an overview of blacklisted strings.
 */
function atrblacklist_result($review) {
  drupal_add_js(drupal_get_path('module', 'atr') . '/js/atr.js');
  drupal_add_css(drupal_get_path('module', 'atr') . '/css/atr.css');

  $header = array(
    array(
      'data' => t('String'),
      'field' => 's.string',
    ),
    array(
      'data' => t('Keyword'),
      'field' => 'sb.keyword',
    ),
  );

  $rows = array();
  $result = pager_query("SELECT s.sid, s.string, sb.keyword, b.comment FROM {atrblacklist_string} sb LEFT JOIN {atr_string} s ON sb.sid = s.sid LEFT JOIN {atrblacklist_keyword} b ON sb.keyword = b.keyword WHERE rid = %d" . tablesort_sql($header), 25, 0, NULL, $review->rid);
  while ($string_data = db_fetch_array($result)) {
    list($sid, $string, $keyword, $comment) = array_values($string_data);
    $options = array(
      'pattern' => array(atrblacklist_pattern($keyword)),
      'comment' => array(filter_xss($comment)),
      'attributes' => array(array(
        'class' => 'error'
        ),),
    );
    $rows[] = array(
      theme('atr_string', atr_string_highlight(htmlspecialchars($string), $options), atr_string_locations($sid)),
      $keyword,
    );
  }

  $pager = theme('pager');

  return $pager . theme_table($header, $rows) . $pager;
}