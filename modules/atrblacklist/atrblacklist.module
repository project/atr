<?php

/**
 * @file
 *   Hook implementations and general funcions.
 */

/**
 * Implementation of hook_theme().
 */
function atrblacklist_theme() {
  return array(
    'atrblacklist_keywords' => array(
      'arguments' => array(
        'element' => NULL,
      ),
    ),
  );
}

/**
 * Implementation of hook_elements().
 */
function atrblacklist_elements() {
  return array(
    'atrblacklist_keywords' => array(
      '#input' => TRUE,
      '#process' => array('atrblacklist_form_process_atrblacklist_keywords'),
    ),
  );
}

/**
 * Implementation of hook_atr_method_info().
 */
function atrblacklist_atr_method_info() {
  return array(
    'blacklist' => array(
      '#title' => t('Blacklist review'),
      '#description' => t('Match all texts against a blacklist.'),
      '#result_callback' => array(
        '#callback' => 'atrblacklist_result',
        '#module' => 'atrblacklist',
        '#file' => 'includes/atrblacklist.result.inc',
      ),
      '#result_count_callback' => array(
        '#callback' => 'atrblacklist_result_count',
        '#module' => 'atrblacklist',
        '#file' => 'includes/atrblacklist.result.inc',
      ),
      '#review_callback' => array(
        'zxx' => array(
          '#callback' => 'atrblacklist_review',
          '#module' => 'atrblacklist',
          '#file' => 'includes/atrblacklist.review.inc',
        ),
      ),
    ),
  );
}

/**
 * Implementation of hook_form_FORM_ID_alter().
 */
function atrblacklist_form_atr_form_profile_alter(&$form, &$form_state) {
  $profile = $form['profile']['#value'];

  $form['#submit'][] = 'atrblacklist_atr_form_profile_submit';
  $keywords = array();
  $result = db_query("SELECT keyword, comment FROM {atrblacklist_keyword} WHERE pid = %d", $profile->pid);
  while ($keyword = db_fetch_object($result)) {
    $keywords[] = $keyword;
  }
  $form['blacklist']['settings']['blacklist_keywords'] = array(
    '#type' => 'atrblacklist_keywords',
    '#description' => t('Any words or phrases that should not exist in the texts with additional comments, i.e. possible improvements.'),
    '#default_value' => $keywords,
  );
  $form['blacklist']['settings']['blacklist_update'] = array(
    '#type' => 'submit',
    '#value' => t('Update keywords'),
  );
}

/**
 * Implementation of hook_atr_profile_delete().
 */
function atrblacklist_atr_profile_delete($pid) {
  db_query("DELETE FROM {atrblacklist_keyword} WHERE pid = %d", $pid);
}

/**
 * Implementation of hook_atr_review_delete().
 */
function atrblacklist_atr_review_delete($rid) {
  db_query("DELETE FROM {atrblacklist_string} WHERE sid IN (SELECT sid FROM {atr_string} s WHERE rid = %d)", $rid);
}

/**
 * Form submit handler for atr_form_profile().
 */
function atrblacklist_atr_form_profile_submit($form, &$form_state) {
  // Delete possible old keywords and save the new ones.
  $values = $form_state['values'];
  $profile = $values['profile'];
  db_query("DELETE FROM {atrblacklist_keyword} WHERE pid = %d", $profile->pid);
  $i = 0;
  $keyword_values = array();
  while (isset($values['blacklist_keywords']['blacklist_keyword_' . $i])) {
    if (!empty($values['blacklist_keywords']['blacklist_keyword_' . $i])) {
      $keyword_values[] = $profile->pid;
      $keyword_values[] = $values['blacklist_keywords']['blacklist_keyword_' . $i];
      $keyword_values[] = $values['blacklist_keywords']['blacklist_comment_' . $i];
    }

    $i++;
  }
  if($count = count($keyword_values) / 3) {
    $placeholders = implode(',', array_fill(0, $count, "(%d, '%s', '%s')"));
    db_query("INSERT INTO {atrblacklist_keyword} VALUES " . $placeholders, $keyword_values);
  }
}

/**
 * Form process handler for atrblacklist_keywords elements.
 */
function atrblacklist_form_process_atrblacklist_keywords($element, $form_state) {
  $i = -1;
  if (count($element['#default_value'])) {
    foreach ($element['#default_value'] as $i => $keyword) {
      $element['blacklist_keyword_' . $i] = array(
        '#type' => 'textfield',
        '#default_value' => $keyword->keyword,
      );
      $element['blacklist_comment_' . $i] = array(
        '#type' => 'textarea',
        '#default_value' => $keyword->comment,
        '#rows' => 1,
      );
    }
  }
  foreach (range($i + 1, $i + 3) as $j) {
    $element['blacklist_keyword_' . $j] = array(
      '#type' => 'textfield',
    );
    $element['blacklist_comment_' . $j] = array(
      '#type' => 'textarea',
      '#rows' => 1,
    );
  }
  $element['#tree'] = TRUE;

  return $element;
}

/**
 * Form theme handler; theme an atrblacklist_keywords element.
 */
function theme_atrblacklist_keywords($element) {
  $i = 0;
  $rows = array();
  while (isset($element['blacklist_keyword_' . $i])) {
    $rows[] = array(
      theme('textfield', $element['blacklist_keyword_' . $i]),
      theme('textarea', $element['blacklist_comment_' . $i]),
    );

    $i++;
  }
  $header = array(t('Keyword'), t('Comment'));

  return theme('form_element', $element, theme('table', $header, $rows));
}

/**
 * Return the blacklist PCRE pattern.
 *
 * @param $keyword
 *   The keyword to match.
 *
 * @return
 *   The PCRE pattern.
 */
function atrblacklist_pattern($keyword) {
  return '#(^|(?<=[^a-z]))' . preg_quote($keyword) . '(?=([^a-z]|$))#i';
}