<?php

/**
 * @file
 *   Installation and uninstallation functions.
 */

/**
 * Implementation of hook_schema().
 */
function atr_schema() {
  return array(
    'atr_profile' => array(
      'description' => 'The settings profiles.',
      'fields' => array(
        'pid' => array(
          'description' => 'A unique ID for every profile.',
          'type' => 'serial',
        ),
        'title' => array(
          'description' => 'The title of this profile.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
        'language' => array(
          'description' => 'Which language this profile has been configured for.',
          'type' => 'varchar',
          'length' => 7,
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('pid'),
    ),
    'atr_profile_method' => array(
      'description' => 'The methods that are used for eveyr {atr_profile}.',
      'fields' => array(
        'pid' => array(
          'description' => 'The {atr_profile}.pid of this record.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'method' => array(
          'description' => 'A review method that is used with this profile.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
      ),
      'indexes' => array(
        'pid' => array('pid'),
      ),
    ),
    'atr_review' => array(
      'description' => 'The base table for reviews.',
      'fields' => array(
        'rid' => array(
          'description' => 'A unique ID for every review.',
          'type' => 'serial',
        ),
        'timestamp' => array(
          'description' => 'A Unix timestamp indicating when this review was run.',
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0
        ),
        'title' => array(
          'description' => 'The title of this review.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
        'language' => array(
          'description' => 'Which language the texts of this review are in.',
          'type' => 'varchar',
          'length' => 7,
          'not null' => TRUE,
        ),
        'string_count' => array(
          'description' => 'The amount of strings of this review.',
          'type' => 'int',
          'size' => 'small',
          'unsigned' => TRUE,
          'default' => 0,
        ),
        'word_count' => array(
          'description' => 'The amount of words of this review.',
          'type' => 'int',
          'size' => 'medium',
          'unsigned' => TRUE,
          'default' => 0,
        ),
      ),
      'indexes' => array(
        'rid' => array('rid'),
      ),
      'primary key' => array('rid'),
    ),
    'atr_review_method' => array(
      'description' => 'The methods used for each {atr_review}.',
      'fields' => array(
        'rid' => array(
          'description' => 'The {atr_review}.rid of the review.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'method' => array(
          'description' => 'A review method that is used with this profile.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
      ),
      'indexes' => array(
        'rid' => array('rid'),
      ),
    ),
    'atr_string' => array(
      'description' => 'The strings that are found in the files to review.',
      'fields' => array(
        'sid' => array(
          'description' => 'A unique ID for every string.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'rid' => array(
          'description' => 'The {atr_review}.rid of the review this string belongs to.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'string' => array(
          'description' => 'A string found in the files to review.',
          'type' => 'text',
          'size' => 'big',
          'not null' => TRUE,
        ),
        'string_lower' => array(
          'description' => 'The lowercase version of the string.',
          'type' => 'text',
          'size' => 'big',
          'not null' => TRUE,
        ),
      ),
      'indexes' => array(
        'sid' => array('sid'),
      ),
      'primary key' => array('sid'),
    ),
    'atr_string_id' => array(
      'description' => 'Generate IDs for all strings from a review.',
      'fields' => array(
        'sid' => array(
          'description' => 'A unique ID for every string.',
          'type' => 'serial',
        ),
        'rid' => array(
          'description' => 'The {atr_review}.rid of the review this string ID belongs to.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
      ),
      'indexes' => array(
        'rid' => array('rid'),
      ),
      'primary key' => array('sid'),
    ),
    'atr_string_location' => array(
      'description' => 'Stores in which files and on which lines every {atr_string} is used.',
      'fields' => array(
        'sid' => array(
          'description' => 'The {atr_string}.sid of this record.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'file' => array(
          'description' => 'The file in which the string was found.',
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
        'line' => array(
          'description' => 'The line of the file the string was found on.',
          'type' => 'int',
          'size' => 'small',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
      ),
      'indexes' => array(
        'sid' => array('sid'),
      ),
    ),
  );
}

/**
 * Implementation of hook_install().
 */
function atr_install() {
  drupal_install_schema('atr');
}

/**
 * Implementation of hook_uninstall().
 */
function atr_uninstall() {
  drupal_uninstall_schema('atr');

  db_query("DELETE FROM {variable} WHERE name LIKE 'atr_%%'");
  cache_clear_all('variables', 'cache');
}