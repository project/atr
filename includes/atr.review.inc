<?php

/**
 * @file
 *   Review strings.
 */

/**
 * Execute a full text review.
 *
 * @param $path
 *   The path to the directory with the files that need to be reviewed.
 * @param $profile
 *   The settings profile used for this review.
 */
function atr_review($review, $profile) {
  // Set up the batch process to execute the review. If this function is called
  // from outside a form submit handler, the batch should be executed manually.
  foreach($review->methods as $method) {
    $operations[] = array(
      'atr_callback_execute',
      array(
        $method['#review_callback'],
        $review,
        $profile,
      ),
    );
  }
  batch_set(array(
    'title' => t('Reviewing texts'),
    'operations' => $operations,
    'file' => drupal_get_path('module', 'atr') .'/includes/atr.review.inc',
  ));
}