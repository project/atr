<?php

/**
 * @file
 *   Extract and save strings.
 */

/**
 * Store strings for a review.
 *
 * @param $rid
 *   The RID of this review.
 * @param $strings
 *   An array with the strings to store.
 */
function atr_store($rid, $strings) {
  if ($count = count($strings)) {
    $values = implode(',', array_fill(0, $count, "($rid)"));
    db_query("INSERT INTO {atr_string_id} (rid) VALUES " . $values);
    $result = db_query("SELECT sid FROM {atr_string_id} WHERE rid = %d ORDER BY sid ASC", $rid);
    $ids = array();
    while ($id = db_result($result)) {
      $ids[] = $id;
    }
    db_query("DELETE FROM {atr_string_id} WHERE rid = %d", $rid);
  
    $atr_string_values = $atr_string_placeholders = $atr_string_location_values = $atr_string_location_placeholders = array();
    $i = 0;
    foreach ($strings as $string => $locations) {
      $id = $ids[$i];
      $atr_string_values[] = $string;
      $atr_string_values[] = $string;
      $atr_string_placeholders[] = "($id, $rid, '%s', LOWER('%s'))";
      foreach ($locations as $location) {
        $atr_string_location_values[] = $location['file'];
        $atr_string_location_values[] = $location['line'];
        $atr_string_location_placeholders[] = "($id, '%s', '%s')";
      }
      $i++;
    }
    db_query("INSERT INTO {atr_string} () VALUES " . implode(',', $atr_string_placeholders), $atr_string_values);
    db_query("INSERT INTO {atr_string_location} VALUES " . implode(',', $atr_string_location_placeholders), $atr_string_location_values);
  }
}

/**
 * Extract and delete a *.tar.gz archive.
 *
 * @param $path
 *   The path to the file.
 */
function atr_targz_extract($path) {
  if (atr_app_exists('tar')) {
    $tar = variable_get('atr_tar_binary', 'tar');
    $dir = dirname($path);
    exec("$tar xzf $path -C $dir");
    unlink($path);
    return TRUE;
  }

  drupal_set_message(t('Could not extract the *.tar.gz file.'), 'error');
  return FALSE;
}

/**
 * Check if a file is a *.tar.gz file.
 *
 * @param $path
 *   The path fo the file.
 *
 * @return
 *   Boolean.
 */
function atr_is_targz($path) {
  $file = fopen($path, 'rb');
  $magic = fread($file, 2);
  fclose($file);

  return $magic == "\37\213";
}

/**
 * Get a file and put it in a temporary directory.
 *
 * @param $path
 *   The path to the file.
 * @param $filename
 *   The name of the file.
 */
function atr_file_get(&$path, $filename) {
  $file = @fopen($path, 'rb');
  if ($file && $dir = atr_tmp_dir()) {
    $dest = "$dir/$filename";
    file_put_contents($dest, $file);
    fclose($file);
    if (atr_is_targz($path) && !atr_targz_extract($dest)) {
      unset($path);
    }
    $path = $dir;
  }
}

/**
 * Recursively remove a directory.
 *
 * This function tries to remove as much of a directory's contents as possible,
 * even if it encountered errors. Therefore FALSE being returned doesn't
 * necessarily mean nothing has been removed at all.
 *
 * @param $path
 *   The path to the directory.
 * @param $errors
 *   Whether the function has encountered any errors. Internal use only.
 *
 * @return
 *   TRUE on succes, FALSE on failure.
 */
function atr_rmdir($path, &$errors = FALSE) {
  $path = '/' . trim($path, '/');

  if (!($dir = @opendir($path))) {
    $errors = TRUE;
  }

  while (($child = @readdir($dir)) !== FALSE) {
    if ($child != '.' && $child != '..') {
      $child = "$path/$child";
      if (is_dir($child)) {
        atr_rmdir($child, $errors);
      }
      elseif(!unlink($child)) {
        $errors = TRUE;
      }
    }
  }

  if ($dir) {
    @closedir($dir);
    if(!@rmdir($path)) {
      $errors = TRUE;
    }
  }

  if ($errors) {
    watchdog('atr', 'Automated Text Review could not remove %directory', array('%directory' => $path), WATCHDOG_WARNING);
  }

  return !$errors;
}

/**
 * Convert filepaths from extracted strings to paths relative to the folder
 * scanned for files.
 *
 * @param $strings
 *   The extracted strings. Structure is similar to global $atr_string.
 * @param $root
 *   The path to the folder that has been scanned for files.
 *   
 */
function atr_filepath(&$strings, $root) {
  $offset = strlen($root);
  foreach ($strings as $string => &$locations) {
    foreach ($locations as &$location) {
      $location['file'] = substr($location['file'], $offset);
    }
  }
}