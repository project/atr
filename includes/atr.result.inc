<?php

/**
 * @file
 *   Review result pages.
 */

/**
 * Display a paged list of all reviews.
 */
function atr_review_list(){
  $result = pager_query("SELECT * FROM {atr_review} ORDER BY timestamp DESC", 25);
  $rows = array();
  while ($review = db_fetch_object($result)) {
    $rows[] = array(l($review->title, 'atr/review/' . $review->rid), format_date($review->timestamp));
  }

  if (count($rows)) {
    $header = array(t('Review'), t('Date'));
    $pager = theme('pager');
    return $pager . theme('table', $header, $rows) . $pager;  
  }
  else {
    return t('No reviews have yet been run.');
  }
}

/**
 * Display review results.
 *
 * @param $review
 *   The review to display results for.
 *
 * @return
 *   The result of the method's #review_callback.
 */
function atr_review_result($review) {
  drupal_set_title(t('Review results for %review', array('%review' => $review->title)));

  $rows = array(
    array(
      t('Date of review'),
      format_date($review->timestamp),
    ),
    array(
      t('Texts'),
      $review->string_count,
    ),
    array(
      t('Words'),
      $review->word_count,
    ),
  );
  $method_info = atr_method_info();
  $methods = array();
  foreach (array_keys($review->methods) as $method) {
    $count = atr_callback_execute($review->methods[$method]['#result_count_callback'], $review->rid);
    $title = $method_info[$method]['#title'] . ' (' . $count['text'] . ')';
    $methods[] = $count['number'] > 0 ? l($title, 'atr/review/' . $review->rid . '/' . $method) : $title;
  }
  $rows[] = array(
    t('Results'),
    theme('item_list', $methods),
  );

  return theme('table', array(), $rows);
}

/**
 * Display review results for a specific method.
 *
 * @param $review
 *   The review to display results for.
 * @param $method
 *   The method to display results for.
 */
function atr_review_method_result($review, $method) {
  $count = atr_callback_execute($review->methods[$method]['#result_count_callback'], $review->rid);
  if ($count['number'] == 0) {
    return drupal_not_found();
  }

  $method_info = atr_method_info($method);
  drupal_set_title(t('%method results for %review', array('%method' => $method_info['#title'], '%review' => $review->title)));
  drupal_set_breadcrumb(array(
    l(t('Home'), '<front>'),
    l(t('Automated Text Review'), 'atr'),
    l(t('Reviews'), 'atr/review'),
    l($review->title, 'atr/review/' . $review->rid),
  ));

  return atr_callback_execute($method_info['#result_callback'], $review);
}

/**
 * Find all locations of a certain string.
 *
 * @param $sid
 *   The SID of the string to find locations for.
 */
function atr_string_locations($sid) {
  static $locations = array();

  if (!isset($locations[$sid])) {
    $result = db_query("SELECT * FROM {atr_string_location} WHERE sid = %d", $sid);
    while ($location = db_fetch_object($result)) {
      $locations[$sid][$location->file][] = $location->line;
    }
  }

  return $locations[$sid];
}

/*
 * Theme a string together with extra information about it.
 *
 * @param $string
 *   The string to theme.
 * @param $locations
 *   An array containing $string's locations from atr_string_locations().
 */
function theme_atr_string($string, $locations) {
  foreach ($locations as $file => $lines) {
    $items[] = format_plural(count($lines), '%file on line !lines', '%file on lines !lines', array('%file' => $file, '!lines' => implode(', ', $lines)));
  }
  $hide = count($items) > 1 ? ' js-hide' : '';

  return $string . theme('item_list', $items, NULL, 'ul', array('class' => 'atr-locations description' . $hide));
}

/**
 * Highlight part(s) of a string.
 *
 * @param $string
 *   The string to highlight part(s) of.
 * @param $options
 *   An array containing at least 'keywords' or 'patterns' for every item:
 *   - keyword: an array containing words or phrases to highlight.
 *   - attributes: HTML attributes to apply to the highlights.
 *   - pattern: a custom PCRE to use instead matching against the keyword.
 *   - comment: a comment for the highlight, by default rendered as titles.
 *   The arrays' items are grouped by their index.
 *
 * @return
 *   The string with all matches highlighted.
 */
function atr_string_highlight($string, array $options) {
  global $atr_string_highlight_comment;
  global $atr_string_highlight_attributes;

  $options += array(
    'keyword' => array(),
    'pattern' => array(),
    'attributes' => array(),
    'comment' => array(),
  );
  $deltas = array_merge(array_keys($options['keyword']), array_keys($options['pattern']));
  foreach ($deltas as $delta) {
    $pattern = isset($options['pattern'][$delta]) ? $options['pattern'][$delta] : '#' . preg_quote($options['keyword'][$delta]) . '#';
    $atr_string_highlight_comment = isset($options['comment'][$delta]) ? $options['comment'][$delta] : NULL;
    $atr_string_highlight_attributes = isset($options['attributes'][$delta]) ? $options['attributes'][$delta] : array();
    $string = preg_replace_callback($pattern, '_atr_string_highlight', $string);
  }

  return $string;
}

/**
 * Helper function for preg_replace_callback() in atr_string_highlight().
 */
function _atr_string_highlight(array $matches) {
  global $atr_string_highlight_comment;
  global $atr_string_highlight_attributes;

  return theme('atr_string_highlight', $matches[0], $atr_string_highlight_comment, $atr_string_highlight_attributes);
}

/**
 * Highlight (part of) a string.
 *
 * @param $string
 *   The (part of the) string to highlight.
 *
 * @return
 *   The highlighted (part of the) string.
 */
function theme_atr_string_highlight($string, $comment = NULL, $attributes = array()) {
  $attributes['class'] = isset($attributes['class']) ? $attributes['class'] . ' atr-highlight' : 'atr-highlight';
  if ($comment) {
    $attributes['title'] = $comment;
    $attributes['class'] .= ' atr-comment';
  }
  return '<span' . drupal_attributes($attributes) . '>' . $string . '</span>';
}

/**
 * Form builder; delete a review.
 */
function atr_form_review_delete(&$form_state, $review = NULL) {
  $form['#redirect'] = 'atr/review';
  $form['review'] = array(
    '#type' => 'value',
    '#value' => $review,
  );

  return confirm_form($form, t('Are you sure you want to delete %review?', array('%review' => $review->title)), 'atr/review/' . $review->rid, NULL, t('Delete'));
}

/**
 * Form submit handler for atr_form_review_delete().
 */
function atr_form_review_delete_submit($form, &$form_state) {
  $review = $form_state['values']['review'];
  atr_review_delete($review->rid);
  drupal_set_message(t('%review has been deleted.', array('%review' => $review->title)));
}