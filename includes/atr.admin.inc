<?php

/**
 * @file
 *   Administration interface.
 */

/**
 * Form builder; general settings.
 */
function atr_form_settings() {
  $form['atr_tar_binary'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to GNU tar'),
    '#default_value' => variable_get('atr_tar_binary', 'tar'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Form builder; manually configure and start a text review.
 */
function atr_form_review() {
  drupal_add_js(drupal_get_path('module', 'atr') . '/js/atr.js');
  drupal_add_css(drupal_get_path('module', 'atr') . '/css/atr.css');

  $form = array(
    // The form may contain file uploads.
    '#attributes' => array('enctype' => "multipart/form-data"),
  );

  // If certain conditions haven't been met, disable the submit button.
  $disabled = FALSE;

  // Source selector.
  $source_info = module_invoke_all('atr_source_info');
  if (count($source_info)) {
    // Render a fieldset and checkbox for every text source.
    foreach ($source_info as $name => $source) {
      $form[$name] = array(
        '#type' => 'fieldset',
        '#title' => $source,
      );
      $form[$name][$name . '_toggle'] = array(
        '#type' => 'checkbox',
        '#title' => t('Extract texts from @text_source.', array('@text_source' => $source)),
        '#attributes' => array(
          'class' => 'atr-settings-toggle',
        ),
      );
      $form[$name]['settings'] = array(
        '#prefix' => '<div class="edit-' . $name . '-toggle js-hide">',
        '#suffix' => '</div>',
      );
    }
  }
  else {
    drupal_set_message(t('You have yet no text sources to use for this review. Please <a href="!modules">enable modules</a> that provide them.', array('!modules' => url('admin/build/modules'))), 'warning');
    $disabled = TRUE;
  }

  // Settings profile selector.
  $result = db_query("SELECT pid FROM {atr_profile} ORDER BY title ASC");
  $profile_options = array();
  $profile_language = array();
  while ($profile = atr_profile_load(db_result($result))) {
    if ($profile->methods) {
      $title = theme('atr_profile', $profile);
      $profile_options[$profile->pid] = $title;
      $profile_language[$profile->pid] = $profile->language;
    }
  }
  $first_profile_pid = end(array_flip(array_reverse($profile_options, TRUE)));
  $count = count($profile_options);
  if ($count > 1) {
    $form['pid'] = array(
      '#type' => 'radios',
      '#title' => t('Settings profile'),
      '#options' => $profile_options,
      '#default_value' => $first_profile_pid,
    );
  }
  elseif ($count == 1) {
    $form['pid'] = array(
      '#type' => 'value',
      '#value' => array_pop(array_flip($profile_options)),
    );
    $form['profile_label'] = array(
      '#type' => 'item',
      '#title' => t('Settings profile'),
      '#description' => t('<a href="@settings">Settings profiles</a> define how to review texts.', array('@settings' => url('atr/profile'))),
      '#value' => array_pop($profile_options),
    );
  }
  else {
    drupal_set_message(t('You have yet no <a href="@profile_add">settings profile</a> to use for this review.', array('@profile_add' => url('atr/profile/add'))), 'warning');
    $disabled = TRUE;
  }

  $languages = atr_languages();
  if (!in_array('zxx', $profile_language)) {
    $languages = array_intersect_key($languages, array_flip($profile_language));
  }
  drupal_add_js(array('atrProfileLanguage' => $profile_language), 'setting');
  $form['language'] = array(
    '#type' => 'select',
    '#title' => t('Text language'),
    '#options' => array_merge(array('zxx' => '<' . t('none') . '>'), $languages),
    '#default_value' => $profile_language[$first_profile_pid],
    '#required' => TRUE,
    '#disabled' => $profile_language[$first_profile_pid] != 'zxx',
  );

  $form['start'] = array(
    '#type' => 'submit',
    '#value' => t('Start review'),
    '#disabled' => $disabled,
  );

  return $form;
}

/**
 * Form validate handler for atr_form_review().
 */
function atr_form_review_validate($form, &$form_state) {
  if ($form_state['values']['language'] == 'zxx') {
    // We reuse existing strings.
    form_set_error('language', t('!name field is required.', array('!name' => t('Text language'))));
  }
}

/**
 * Form submit handler for atr_form_review().
 */
function atr_form_review_submit($form, &$form_state) {
  global $atr_strings;

  // Proceed with the review only if strings have been extracted (by other
  // submit handlers).
  if (count($atr_strings)) {
    $profile = atr_profile_load((int) $form_state['values']['pid']);
    $language = $profile->language == 'zxx' ? $form_state['values']['language'] : $profile->language;
    $review = new ATRReview('', $language, $profile->methods);
    atr_review_save($review);

    // Store the extracted strings.
    module_load_include('inc', 'atr', 'includes/atr.extract');
    atr_store($review->rid, $atr_strings);

    // Save additional review information.
    $review->title = t('Review !review_id', array('!review_id' => $review->rid));
    $review->string_count = count($atr_strings);
    foreach (array_keys($atr_strings) as $string) {
      $review->word_count += count(preg_split('#\s#', $string));
    }
    atr_review_save($review);

    // Execute the actual review.
    module_load_include('inc', 'atr', 'includes/atr.review');
    atr_review($review, $profile);
    $form_state['redirect'] = 'atr/review/' . $review->rid;
    drupal_set_message(t('The texts have been reviewed.'));
  }
  else {
    drupal_set_message(t('No texts have been found.'), 'warning');
  }
}

/**
 * List all settings profiles.
 */
function atr_profile_list() {
  $result = db_query("SELECT pid FROM {atr_profile} ORDER BY title ASC");
  $rows = array();
  while ($profile = atr_profile_load(db_result($result))) {
    $rows[] = array(theme('atr_profile', $profile), l(t('edit'), "atr/profile/$profile->pid/edit"), l(t('delete'), "atr/profile/$profile->pid/delete"));
  }

  if ($rows) {
    $header = array(
      array(
        'data' => t('Profile'),
        'colspan' => 3,
      ),
    );
    return theme('table', $header, $rows);
  }
  else {
    return t('You have yet no settings profiles.');
  }
}

/**
 * Form builder; add a settings profile.
 */
function atr_form_profile_add(&$form_state) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
  );
  $form['language'] = array(
    '#type' => 'select',
    '#title' => t('Text language'),
    '#options' => atr_languages_profile(),
    '#default_value' => 'zxx',
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save profile'),
  );

  return $form;
}

/**
 * Form submit handler for atr_form_profile_add().
 */
function atr_form_profile_add_submit($form, &$form_state) {
  $values = $form_state['values'];
  $profile = (object) array(
    'title' => $values['title'],
    'language' => $values['language'],
    'methods' => array(),
  );
  atr_profile_save($profile);
  drupal_set_message(t('%profile has been added.', array('%profile' => $profile->title)));

  $form_state['redirect'] = 'atr/profile/' . $profile->pid . '/edit';
}

/**
 * Form builder; edit a settings profile.
 */
function atr_form_profile(&$form_state, $profile = NULL) {
  include_once './includes/locale.inc';
  drupal_add_js(drupal_get_path('module', 'atr') . '/js/atr.js');

  $form['#redirect'] = 'atr/profile';
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $profile->title,
    '#required' => TRUE,
  );
  $form['language'] = array(
    '#type' => 'item',
    '#title' => t('Text language'),
    '#value' => atr_language_profile($profile->language),
  );

  // Render a fieldset and 'Enable' checkbox for every review method.
  $methods_info = module_invoke_all('atr_method_info');
  foreach (array_keys($methods_info) as $method) {
    if ($method_info = atr_method_availability($method, $profile->language)) {
      $form[$method] = array(
        '#type' => 'fieldset',
        '#title' => $method_info['#title'],
        '#description' => isset($method_info['#description']) ? $method_info['#description'] : NULL,
      );
      $form[$method][$method . '-toggle'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable @review_method', array('@review_method' => $method_info['#title'])),
        '#default_value' => in_array($method, $profile->methods),
        '#attributes' => array(
          'class' => 'atr-settings-toggle',
        ),
      );
    }
    $form[$method]['settings'] = array(
      '#prefix' => '<div class="edit-' . $method . '-toggle js-hide">',
      '#suffix' => '</div>',
    );
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save profile'),
  );
  $form['profile'] = array(
    '#type' => 'value',
    '#value' => $profile,
  );

  return $form;
}

/**
 * Form submit handler for atr_form_profile().
 */
function atr_form_profile_submit($form, &$form_state) {
  $values = &$form_state['values'];

  // Rebuild the form if the keywords need to be updated.
  if ($values['op'] == t('Update keywords')) {
    $form_state['rebuild'] = TRUE;
  }

  // Build and save the $profile object.
  $profile = &$values['profile'];
  $profile->title = $values['title'];
  $method_info = module_invoke_all('atr_method_info');
  $profile->methods = array();
  foreach (array_keys($method_info) as $method) {
    if (isset($values[$method . '-toggle']) && $values[$method . '-toggle']) {
      $profile->methods[] = $method;
    }
  }
  atr_profile_save($profile);
  drupal_set_message(t('%profile has been updated.', array('%profile' => $profile->title)));
}

/**
 * Form builder; delete a settings profile.
 */
function atr_form_profile_delete(&$form_state, $profile = NULL) {
  $form['#redirect'] = 'atr/profile';
  $form['profile'] = array(
    '#type' => 'value',
    '#value' => $profile,
  );

  return confirm_form($form, t('Are you sure you want to delete %profile?', array('%profile' => $profile->title)), 'atr/profile', NULL, t('Delete'));
}

/**
 * Form submit handler for atr_form_settings_delete().
 */
function atr_form_profile_delete_submit($form, &$form_state) {
  $profile = $form_state['values']['profile'];
  atr_profile_delete($profile->pid);

  drupal_set_message(t('%profile has been deleted.', array('%profile' => $profile->title)));
}

/**
 * Delete a settings profile.
 *
 * @param $pid
 *   The PID of the profile to delete.
 */
function atr_profile_delete($pid) {
  module_invoke_all('atr_profile_delete', $pid);
  db_query("DELETE FROM {atr_profile} WHERE pid = %d", $pid);
}

/**
 * Return a list of languages.
 *
 * @return
 *   Array where the keys are languages codes and values are language names.
 */
function atr_languages() {
  static $languages = NULL;
  include_once './includes/locale.inc';

  if(!$languages) {
    $languages = _locale_get_predefined_list();
    foreach ($languages as $code => $names) {
      // Include native name in output, if possible
      $name = t($names[0]);
      $languages[$code] = $name;
      if (count($names) > 1 && $name != $names[1]) {
        $languages[$code] .= ' (' . $names[1] . ')';
      }
    }
    asort($languages);
  }

  return $languages;
}

/**
 * Return a language name based on a language code.
 *
 * @param $code
 *   A language code as defined in _locale_get_predefined_list().
 *
 * @return
 *   The language name.
 */
function atr_language($code) {
  $languages = atr_languages();

  return $languages[$code];
}

/**
 * Return a list of languages.
 *
 * @return
 *   Array where the keys are languages codes and values are language names.
 */
function atr_languages_profile() {
  return array_merge(array('zxx' => t('Any language')), atr_languages());
}

/**
 * Return a language name based on a language code.
 *
 * @param $code
 *   A language code as defined in _locale_get_predefined_list() or 'all'.
 *
 * @return
 *   The language name.
 */
function atr_language_profile($code) {
  $languages = atr_languages_profile();

  return $languages[$code];
}

/**
 * Theme a settings profile for display.
 *
 * @param $profile
 *   The profile to theme.
 *
 * @return
 *   String.
 */
function theme_atr_profile($profile) {
  $methods_info = array_intersect_key(atr_method_info(), array_flip($profile->methods));
  $methods = array();
  foreach ($methods_info as $method_info) {
    $methods[] = $method_info['#title'];
  }
  sort($methods);
  $methods = ' (' . ($methods ? implode(', ', $methods) : t('This profile has no review methods')) . ')';
  
  return $profile->title . '<div class="description">' . atr_language_profile($profile->language) . $methods . '</div>';
}