<?php

/**
 * @file
 *   API documentation.
 */

/**
 * Define review methods.
 *
 * All properties except '#description' are required.
 *
 * @return
 *   An associative array.
 */
function hook_atr_method_info() {
  return array(
    'similarity' => array( // The machine name of the review method.
      '#title' => t('Similarity review'), // The human-readable name of the
      // review method.
      '#description' => t('Compare all strings to eachother and list all that are similar. To reduce the amount of different strings, the ones that are similar may be merged.'),
      '#result_callback' => array( // Callback information that displays review
      // results.
        '#callback' => 'atr_results_similar',
        '#module' => 'atr',
        '#file' => 'includes/atr.results.inc',
      ),
      '#review_callback' => array( // Callback information for triggering the
      // review.
        'zxx' => array( // The code of language these callbacks are for as per ISO
        // 639.
          '#callback' => 'atr_review_similar', // The callback's function name.
          '#module' => 'atr', // The module the callback is in.
          '#file' => 'includes/atr.review.inc', // The file the callback is in.
        ),
      ),
    ),
    'blacklist' => array(
      '#title' => t('Blacklist review'),
      '#description' => t('List all strings that contain blacklisted words.'),
      '#result_callback' => array(
        '#callback' => 'atr_results_blacklist',
        '#module' => 'atr',
        '#file' => 'includes/atr.results.inc',
      ),
      '#review_callback' => array(
        'zxx' => array(
          '#callback' => 'atr_review_blacklist',
          '#module' => 'atr',
          '#file' => 'includes/atr.review.inc',
        ),
      ),
    ),
  );
}

/**
 * Alter review methods as defined in hook_atr_method_info().
 */
function hook_atr_method_info_alter(&$methods) {
  $methods['similarity']['#title'] = t('Hello similarity!');
  $methods['similarity']['#review_callback'] = array(
    '#callback' => 'atr_hello_world',
    '#module' => 'atr',
    '#file' => 'includes/atr.hello.inc',
  );
}

/**
 * Define text sources.
 *
 * @return
 *   An associative array. Every item is a text source. Keys are machine names
 *   and values are human-readable titles of where to extract texts from.
 */
function hook_atr_source_info() {
  return array(
    'atrcode' => t('Drupal code files'),
  );
}

/**
 * Respond to reviews being saved.
 *
 * @param $review
 *   The ATRReview object of the review being saved.
 */
function hook_atr_review_save($review) {
  db_query("INSERT INTO {foo_average} VALUES (%d)", $review->word_count / $review->string_count);
}

/**
 * Respond to reviews being loaded.
 *
 * @param $reviews
 *   A referenced array with the ATRReview objects of the reviews that are being
 *   loaded.
 */
function hook_atr_review_load(&$reviews) {
  foreach ($reviews as $rid => $review) {
    $reviews[$rid]->foo = variable_get('foo_' . $review->language, 'bar');
  }
}

/**
 * Respond to review deletion.
 *
 * @param $rids
 *   An array with the RIDs of the reviews that are being deleted.
 */
function hook_atr_review_delete($rids) {
  $palceholders = db_placeholders($rids);
  db_query("DELETE FROM {atr_review} WHERE rid IN ($placeholders)", $rids);
}

/**
 * Respond to profile deletion.
 *
 * @param $pid
 *   The PID of the profile that is being deleted.
 */
function hook_atr_profile_delete($pid) {
  variable_del('atrsimilar_profile_' . $pid . '_threshold');
}