
(function ($) {
  Drupal.behaviors.atr = function (context) {
    // Toggle settings based on checkbox configuration.
    $('.atr-settings-toggle:checkbox').each(function(){
      if(this.checked) {
        $('.' + this.id).show();
      }
    }).click(function(){
      $('.' + this.id).toggle();
    });
    // Toggle settings based on radio button configuration.
    $('.atr-settings-toggle:radio').each(function(){
      if(this.checked) {
        $('.' + this.name + '-' + this.value).show();
      }
    }).change(function(){
      $('.' + this.name).toggle();
    });
    // Toggle string location visibility.
    $('.atr-locations.js-hide').each(function(){
      $(this).before('<span class="atr-locations-toggle description">' + Drupal.t('Locations') + '</span>');
    });
    $('.atr-locations-toggle').click(function(){
      $(this.parentNode).children('.atr-locations').toggle();
      $(this).toggleClass('expanded');
    });
    // Toggle the review form's language selector.
    $('[name=pid]').click(function(){
      if (Drupal.settings.atrProfileLanguage[this.value] == 'zxx') {
        $('#edit-language').removeAttr('disabled');
      }
      else {
        $('#edit-language').attr('disabled', true).val(Drupal.settings.atrProfileLanguage[this.value]);
      }
    });
  };
})(jQuery);
